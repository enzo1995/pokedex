//
//  ApiManager.swift
//  Pokedex
//
//  Created by enzo corsiero on 16/01/21.
//

import Foundation

class ApiManager {
    static func getPokemonList(url: String? = nil, completion: @escaping(PokemonListResponse?) -> Void) {
        let pokemonUrl: String
        if let url = url {
            pokemonUrl = url
        } else {
            pokemonUrl = Constants.Urls.pokemonList
        }
        RequestManager.networkRequest(urlString: pokemonUrl) { (data, _, error) in
            if let data = data, error == nil {
                if let result = try? JSONDecoder().decode(PokemonListResponse.self, from: data) {
                    completion(result)
                    return
                }
            }
            completion(nil)
        }
    }
    
    static func getPokemonDetails(url: String, completion: @escaping(PokemonDetailsResponse?) -> Void) {
        RequestManager.networkRequest(urlString: url) { (data, _, error) in
            if let data = data, error == nil {
                if let result = try? JSONDecoder().decode(PokemonDetailsResponse.self, from: data) {
                    completion(result)
                    return
                }
            }
            completion(nil)
        }
    }
}
