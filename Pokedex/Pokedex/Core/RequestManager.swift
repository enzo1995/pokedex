//
//  RequestManager.swift
//  Pokedex
//
//  Created by enzo corsiero on 15/01/21.
//

import Foundation
import UIKit

class RequestManager {
    
    static func networkRequest(urlString: String, header: [String: String] = [:], type: ConnectionType = .get, body: Data? = nil, showLoader: Bool = true, showError: Bool = true, completion: @escaping (Data?, URLResponse?, Error?) -> Void) {
        if showLoader {
            MBProgressHUDMessanger.shared.showLoading()
        }
        URLSession.shared.reset {
            #if DEBUG
            print("chache reset")
            #endif
            let url = URL(string: urlString)
            if let usableUrl = url {
                let sessionConfig = URLSessionConfiguration.default
                sessionConfig.timeoutIntervalForRequest = 10.0
                sessionConfig.timeoutIntervalForResource = 10.0
                let session = URLSession(configuration: sessionConfig)
                var request = URLRequest(url: usableUrl)
                request.httpMethod = type.rawValue
                request.addValue("application/json", forHTTPHeaderField: "Content-Type")
                if let body = body, type == .post {
                    request.httpBody = body
                }
                let task = session.dataTask(with: request) { data, response, error in
                    DispatchQueue.main.async {
                        MBProgressHUDMessanger.shared.stopLoading()
                        #if DEBUG
                        if let data = data {
                            let json = try? JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? NSDictionary
                            print("JSON: \n\(String(describing: json))")
                        }
                        #endif
                        completion(data,response,error)
                        if error != nil && showError {
                            SystemAlertMessage.standardAlert(title: Localizer.Alert.opsTitle.localized, message: Localizer.Alert.genericError.localized, confirm: Localizer.Common.retry.localized, cancel: Localizer.Common.okay.localized) { alertResponse in
                                guard alertResponse == .confirmed else { return }
                                RequestManager.networkRequest(urlString: urlString, header: header, type: type, body: body) { data, response, error in
                                    completion(data,response,error)
                                }
                            }
                        }
                    }
                }
                task.resume()
            } else {
                #if DEBUG
                print("----Error URL o params----")
                #endif
                DispatchQueue.main.async {
                    MBProgressHUDMessanger.shared.stopLoading()
                    completion(nil,nil,NetworkError.urlNotValid)
                    if showError {
                        SystemAlertMessage.standardAlert(title: Localizer.Alert.opsTitle.localized, message: Localizer.Alert.genericError.localized, confirm: Localizer.Common.retry.localized, cancel: Localizer.Common.okay.localized) { alertResponse in
                            guard alertResponse == .confirmed else { return }
                            RequestManager.networkRequest(urlString: urlString, header: header, type: type, body: body) { data, response, error in
                                completion(data,response,error)
                            }
                        }
                    }
                }
            }
        }
    }
    
    
    static func networkRequest<T: Encodable>(urlString: String, header: [String: String] = [:], type: ConnectionType = .get, body: T? = nil, showLoader: Bool = true, showError: Bool = true, completion: @escaping (Data?, URLResponse?, Error?) -> Void) {
        let encoder = JSONEncoder()
        encoder.dateEncodingStrategy = .formatted(DateFormatter.iso8601Full)
        do {
            let bodyData = try encoder.encode(body)
            self.networkRequest(urlString: urlString, header: header, type: type, body: bodyData, showLoader: showLoader, showError: showError, completion: completion)
        } catch {
            DispatchQueue.main.async {
                MBProgressHUDMessanger.shared.stopLoading()
            }
            print(error.localizedDescription)
        }
    }
}

public enum ConnectionType: String {
    case get = "GET"
    case post = "POST"
}

public enum NetworkError: Error {
    case urlNotValid
    case dataDeactivate
}

extension NetworkError: LocalizedError {
    public var errorDescription: String? {
        switch self {
        case .urlNotValid:
            return NSLocalizedString("Url not valid", comment: "Network error")
        case .dataDeactivate:
            return NSLocalizedString("Data inactive", comment: "Network error")
        }
    }
}
