//
//  Localizable.swift
//  Pokedex
//
//  Created by enzo corsiero on 15/01/21.
//

import Foundation

public protocol Localizable {
    var localizedKey: String { get }
}

public extension RawRepresentable where RawValue == String, Self: Localizable {
    var localized: String {
        let key = "\(localizedKey).\(String(describing: type(of: self)).lowercased()).\(self)"
        return NSLocalizedString(key, tableName: nil, bundle: Bundle.main, value: String.empty, comment: String.empty)
    }
}
