//
//  Localizer.swift
//  Pokedex
//
//  Created by enzo corsiero on 15/01/21.
//

import Foundation

public extension Localizable {
    var localizedKey: String { return "pokedex" }
}

public enum Localizer {
    
    public enum Common: String, Localizable {
        case title
        case okay
        case retry
    }
    
    public enum Alert: String, Localizable {
        case opsTitle
        case genericError
        
        case loadDetailError
    }
    
    public enum Details: String, Localizable {
        case moves
        case abilities
        case stats
    }
}
