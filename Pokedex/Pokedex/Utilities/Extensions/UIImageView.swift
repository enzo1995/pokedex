//
//  UIImageView.swift
//  Pokedex
//
//  Created by enzo corsiero on 16/01/21.
//

import Foundation
import UIKit

extension UIImageView {
    
    func loadImage(at url: URL) {
        UIImageLoader.loader.load(url, for: self)
      }
      func cancelImageLoad() {
        UIImageLoader.loader.cancel(for: self)
      } 
}
