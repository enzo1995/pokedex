//
//  UIButton.swift
//  Pokedex
//
//  Created by enzo corsiero on 15/01/21.
//

import Foundation
import UIKit

extension UIButton {
    
    func actionButton() {
        self.cleanBorderButton(textColor: .mainColor, borderColor: .mainColor)
        self.titleLabel?.font = FontManager.sharedInstance.boldFontWith(size: Constants.TextSize.ButtonTitle)
    }
    
    func confirmButton() {
        self.borderButton(textColor: .mainWhite, backgroundColor: .mainColor)
        self.titleLabel?.font = FontManager.sharedInstance.boldFontWith(size: Constants.TextSize.ButtonTitle)
    }
    
    func noBoderedButton() {
        self.backgroundColor = .clear
        self.setTitleColor(.mainRed, for: .normal)
        self.titleLabel?.font = FontManager.sharedInstance.boldFontWith(size: Constants.TextSize.ButtonTitle)
    }
    
    func closeButton() {
        self.layer.cornerRadius = self.frame.height / 2
        self.backgroundColor = .mainColor
        self.tintColor = .white
        self.setImage(#imageLiteral(resourceName: "close_x_white"), for: .normal)
        self.imageView?.contentMode = .scaleAspectFit
        self.setTitle(String.empty, for: .normal)
    }
    
    func borderButton(textColor: UIColor, backgroundColor: UIColor) {
        self.heightAnchor.constraint(equalToConstant: Constants.buttonHeight).isActive = true
        self.layer.cornerRadius = Constants.buttonHeight / 2
        self.backgroundColor = backgroundColor
        self.setTitleColor(textColor, for: .normal)
    }
    
    func cleanBorderButton(textColor: UIColor, borderColor: UIColor) {
        self.heightAnchor.constraint(equalToConstant: Constants.buttonHeight).isActive = true
        self.layer.cornerRadius = Constants.buttonHeight / 2
        self.backgroundColor = .mainWhite
        self.layer.borderColor = borderColor.cgColor
        self.layer.borderWidth = 2.0
        self.setTitleColor(textColor, for: .normal)
    }
    
    func setEnabled(_ enabled: Bool) {
        self.alpha = enabled ? 1.0 : 0.5
        self.isEnabled = enabled
    }
    
    func setAlphaBy(enaled: Bool) {
        self.alpha = enaled ? 1.0 : 0.5
    }
}
