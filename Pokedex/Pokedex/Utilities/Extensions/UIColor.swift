//
//  UIColor.swift
//  Pokedex
//
//  Created by enzo corsiero on 15/01/21.
//

import Foundation
import UIKit

extension UIColor {
    
    static let mainColor = UIColor.mainYellow
    static let secondColor = UIColor.lightGray
    static let textColor = UIColor(named: "blackText")
    static let separatorColor = UIColor.lightGray
    
    // MARK: - Status colors
    static let confirmedColor = UIColor.mainYellow
    static let declinedColor = UIColor.mainRed
    
    // MARK: - Black colors
    static let mainBlack = UIColor(red: 32.0 / 255, green: 32.0 / 255, blue: 32.0 / 255, alpha: 1.0)
    static let mainGray = UIColor(red: 90.0 / 255, green: 90.0 / 255, blue: 90.0 / 255, alpha: 1.0)
    static let lightGray = UIColor(red: 180.0 / 255, green: 180.0 / 255, blue: 180.0 / 255, alpha: 1.0)
    static let veryLightGray = UIColor(red: 225.0 / 255, green: 225.0 / 255, blue: 225.0 / 255, alpha: 1.0)
    static let descriptionBlack = UIColor(red: 64.0 / 255, green: 64.0 / 255, blue: 64.0 / 255, alpha: 1.0)
    
    // MARK: - White colors
    static let mainWhite = UIColor(red: 255.0 / 255, green: 255.0 / 255, blue: 255.0 / 255, alpha: 1.0)
    
    // MARK: - Green colors
    static let mainGreen = UIColor(red: 95.0 / 255, green: 164.0 / 255, blue: 150.0 / 255, alpha: 1.0)
    static let lightGreen = UIColor(red: 48.0 / 255, green: 128.0 / 255, blue: 29.0 / 255, alpha: 0.4)
    static let darkGreen = UIColor(red: 0.0 / 255, green: 70.0 / 255, blue: 0.0 / 255, alpha: 1.0)
    
    // MARK: - Red color
    static let mainRed = UIColor(red: 186 / 255, green: 29 / 255, blue: 29 / 255, alpha: 1.0)
    
    // MARK: - Yellow
    static let mainYellow = UIColor(red: 253 / 255, green: 165 / 255, blue: 15 / 255, alpha: 1.0)
}
