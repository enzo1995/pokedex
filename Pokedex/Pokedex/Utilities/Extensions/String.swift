//
//  String.swift
//  Pokedex
//
//  Created by enzo corsiero on 15/01/21.
//

import Foundation

extension String {
    static var empty: String { "" }
}
