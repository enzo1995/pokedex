//
//  Constants.swift
//  Pokedex
//
//  Created by enzo corsiero on 15/01/21.
//

import Foundation
import UIKit

struct Constants {
    static let basePath = ""

    struct Urls {
        static let pokemonList = "https://pokeapi.co/api/v2/pokemon"
        static let pokemonImage = "https://pokeres.bastionbot.org/images/pokemon/%@.png"
    }
    
    
    // MARK: - Viewcontrollers
    struct ViewControllers {
    }
    
    static let buttonHeight: CGFloat = 40.0
    
    // MARK: - Date format
    struct DateFormat {
        static let Time = "HH:mm"
        static let Date = "dd/MM/yyyy"
        static let SingleValueDate = "d/M/yyyy"
        static let SingleDateTime = "d/M/yyyy HH:mm"
        static let DayMonthDate = "d/M"
        static let TextDate = "E, d MMM yyyy"
        static let ServerFormat = "yyyy MM dd HH mm ss"
    }
    
    // MARK: - Text sizes
    struct TextSize {
        static let SuperTitle: CGFloat = 38.0
        static let Title: CGFloat = 24.0
        static let Subtitle: CGFloat = 18.0
        
        static let Description: CGFloat = 16.0
        static let SubDescription: CGFloat = 14.0
        
        static let ButtonTitle: CGFloat = 18.0
    }
    
    struct FontName {
        static let light = ""
        static let regular = "AmericanTypewriter"
        static let medium = "AmericanTypewriter-Semibold"
        static let bold = "AmericanTypewriter-Bold"
    }
    
}
