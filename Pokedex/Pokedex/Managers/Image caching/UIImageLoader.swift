//
//  UIImageLoader.swift
//  Pokedex
//
//  Created by enzo corsiero on 17/01/21.
//

import Foundation
import UIKit

final class UIImageLoader {
    
    static let loader = UIImageLoader()
    private let imageLoader = ImageLoader()
    private var uuidMap = [UIImageView: UUID]()
    private init() {}
    func load(_ url: URL, for imageView: UIImageView) {
        let token = self.imageLoader.loadImage(url) { result in
            defer { self.uuidMap.removeValue(forKey: imageView) }
            
            do {
                let image = try result.get()
                DispatchQueue.main.async {
                    imageView.image = image
                }
            } catch { }
        }
        
        if let token = token {
            self.uuidMap[imageView] = token
        }
    }
    
    func cancel(for imageView: UIImageView) {
        if let uuid = self.uuidMap[imageView] {
            self.imageLoader.cancelLoad(uuid)
            self.uuidMap.removeValue(forKey: imageView)
        }
    }
}
