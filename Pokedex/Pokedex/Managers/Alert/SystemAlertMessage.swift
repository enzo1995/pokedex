//
//  SystemAlertMessage.swift
//  Pokedex
//
//  Created by enzo corsiero on 15/01/21.
//

import Foundation
import UIKit

public final class SystemAlertMessage {
    static func standardAlert(title: String? = nil,
                              message: String? = nil,
                              confirm: String,
                              cancel: String? = nil,
                              dismiss: ((AlertResponse) -> Void)? = nil) {
        let presentVC: StandardAlertViewController = StandardAlertViewController()
        presentVC.setup(title: title, message: message, confirm: confirm, cancel: cancel, completion: dismiss)
        presentVC.modalTransitionStyle = .crossDissolve
        presentVC.modalPresentationStyle = .overCurrentContext
        UIApplication.shared.windows.filter({$0.isKeyWindow}).first?.rootViewController?.lastViewController?.present(presentVC, animated: true)
    }
}
