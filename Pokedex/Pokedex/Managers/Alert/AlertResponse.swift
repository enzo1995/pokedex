//
//  AlertResponse.swift
//  Pokedex
//
//  Created by enzo corsiero on 15/01/21.
//

import Foundation

public enum AlertResponse: Equatable {
    case canceled
    case confirmed
    
    public static func == (lhs: AlertResponse, rhs: AlertResponse) -> Bool {
        switch (lhs, rhs) {
        case (.canceled, .canceled):
            return true
            
        case (.confirmed, .confirmed):
            return true
            
        default:
            return false
        }
    }
}
