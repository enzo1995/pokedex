//
//  StandardAlertViewController.swift
//  Pokedex
//
//  Created by enzo corsiero on 15/01/21.
//

import UIKit

class StandardAlertViewController: UIViewController {
    
    let blackView: UIView = {
        let view = UIView()
        view.backgroundColor = .black
        view.alpha = 0
        return view
    }()
    
    let blurView: UIVisualEffectView = {
        let view = UIVisualEffectView()
        view.effect = UIBlurEffect(style: .dark)
        view.alpha = 0
        return view
    }()
    
    var containerView: UIView = {
        let view = UIView()
        view.alpha = 0.0
        view.backgroundColor = . white
        view.addShadow()
        view.layer.cornerRadius = 8.0
        return view
    }()
    
    var titleLabel: UILabel = {
        let label = UILabel(frame: CGRect.zero)
        label.font = FontManager.sharedInstance.boldFontWith(size: Constants.TextSize.Title)
        label.textColor = .mainColor
        label.textAlignment = .center
        return label
    }()
    
    var descriptionTextView: UITextView = {
        let textView = UITextView()
        textView.font = FontManager.sharedInstance.regularFontWith(size: Constants.TextSize.Description)
        textView.textColor = UIColor.textColor
        textView.backgroundColor = .mainWhite
        textView.textAlignment = .center
        textView.isScrollEnabled = false
        return textView
    }()
    
    var dismissButton: UIButton = {
        let button = UIButton()
        button.actionButton()
        return button
    }()
    
    var confirmButton: UIButton = {
        let button = UIButton()
        button.confirmButton()
        return button
    }()
    
    public typealias CompletionBlock = ((AlertResponse) -> Void)?
    var completionBlock: CompletionBlock?
    
    var titleAlert: String?
    var message: String?
    var confirm: String?
    var dismiss: String?
    
    init() {
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.addBlur()
        self.initContainerView()
        self.initGraphics()
        self.initData()
        
    }
    
    private func addBlur() {
        self.view.addSubview(blackView)
        blackView.anchor(top: view.topAnchor, left: view.leadingAnchor, bottom: view.bottomAnchor, right: view.trailingAnchor, paddingTop: 0, paddingLeft: 0, paddingBottom: 0, paddingRight: 0)
        
        
        self.view.addSubview(blurView)
        blurView.anchor(top: view.topAnchor, left: view.leadingAnchor, bottom: view.bottomAnchor, right: view.trailingAnchor, paddingTop: 0, paddingLeft: 0, paddingBottom: 0, paddingRight: 0)
    }
    
    private func initContainerView() {
        
        let buttonStackView = UIStackView(arrangedSubviews: [self.dismissButton, self.confirmButton])
        buttonStackView.spacing = 32.0
        buttonStackView.alignment = .fill
        buttonStackView.distribution = .fillEqually
        buttonStackView.axis = .horizontal
        buttonStackView.contentMode = .scaleToFill
        
        let stackView = UIStackView(arrangedSubviews: [self.titleLabel, self.descriptionTextView, buttonStackView])
        stackView.spacing = 6.0
        stackView.alignment = .fill
        stackView.distribution = .fill
        stackView.axis = .vertical
        stackView.contentMode = .scaleToFill
        
        self.containerView.addSubview(stackView)
        stackView.anchor(top: containerView.topAnchor, left: containerView.leadingAnchor, bottom: containerView.bottomAnchor, right: containerView.trailingAnchor, paddingTop: 8, paddingLeft: 8, paddingBottom: 8, paddingRight: 8)
        
        
        self.view.addSubview(self.containerView)
        self.containerView.anchor(left: view.leadingAnchor, right: view.trailingAnchor, paddingLeft: 8, paddingRight: 8, centerX: view.centerXAnchor, centerY: view.centerYAnchor)
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        let notification = UINotificationFeedbackGenerator()
        self.containerView.transform = self.containerView.transform.scaledBy(x: 0.001, y: 0.001)
        UIView.animate(withDuration: 0.5, delay: 0.0, usingSpringWithDamping: 0.5, initialSpringVelocity: 0.3, options: .curveEaseInOut, animations: {
            notification.notificationOccurred(.warning)
            self.containerView.alpha = 1.0
            self.blackView.alpha = 0.7
            self.blurView.alpha = 0.7
            self.containerView.transform = CGAffineTransform.identity.scaledBy(x: 1.0, y: 1.0)
        }, completion: nil)
    }
    
    private func initGraphics() {
        
    }
    
    private func initData() {
        self.titleLabel.text = titleAlert
        if let description = message {
            self.descriptionTextView.text = description
        }
        
        self.confirmButton.addTarget(self, action: #selector(confirmAction(_:)), for: .touchUpInside)
        self.dismissButton.addTarget(self, action: #selector(dismissAction(_:)), for: .touchUpInside)
        self.confirmButton.setTitle(confirm, for: .normal)
        if let dismissText = dismiss {
            self.dismissButton.setTitle(dismissText, for: .normal)
        } else {
            self.dismissButton.removeFromSuperview()
        }
    }
    
    public func setup(title: String? = nil,
                      message: String? = nil,
                      confirm: String,
                      cancel: String? = nil,
                      completion: CompletionBlock? = nil) {
        self.titleAlert = title
        self.message = message
        self.confirm = confirm
        self.dismiss = cancel
        self.completionBlock = completion
    }
    
    @IBAction func dismissAction(_ sender: Any) {
        guard let completion = completionBlock else {
            self.dismiss(animated: true, completion: nil)
            return
        }
        self.dismiss(animated: true) {
           completion?(.canceled)
        }
    }
    
    @IBAction func confirmAction(_ sender: Any) {
        guard let completion = completionBlock else {
            self.dismiss(animated: true, completion: nil)
            return
        }
        self.dismiss(animated: true) {
            completion?(.confirmed)
        }
    }
    
}
