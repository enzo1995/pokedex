//
//  FontManager.swift
//  Pokedex
//
//  Created by enzo corsiero on 15/01/21.
//

import Foundation
import UIKit

final class FontManager {
    
    static let sharedInstance = FontManager()
    
    private init() {}
    
    let regularFontName: String = Constants.FontName.regular
    
    let boldFontName: String = Constants.FontName.bold
    
    let mediumFontName: String = Constants.FontName.medium
        
    func regularFontWith(size: CGFloat) -> UIFont {
        UIFont(name: self.regularFontName, size: size)!
    }
    
    func boldFontWith(size: CGFloat) -> UIFont {
        UIFont(name: self.boldFontName, size: size)!
    }
    
    func mediumFontWith(size: CGFloat) -> UIFont {
        UIFont(name: self.mediumFontName, size: size)!
    }
    
}
