//
//  PokemonDetailsResponse.swift
//  Pokedex
//
//  Created by enzo corsiero on 17/01/21.
//

import Foundation

struct PokemonDetailsResponse: Decodable {
    let id: Int?
    let name: String?
    let abilities: [Ability]?
    let moves: [Move]?
    let stats: [Stat]?
}

struct Ability: Decodable {
    let ability: AbilityInfo?
    let isHidden: Bool?
    let slot: Int?
}

struct AbilityInfo: Decodable {
    let name: String?
    let url: String?
}

struct Move: Decodable {
    let move: MoveInfo?
}

struct MoveInfo: Decodable {
    let name: String?
    let url: String?
}

struct Stat: Decodable {
    let base_stat: Int?
    let effort: Int?
    let stat: StatInfo?
}

struct StatInfo: Decodable {
    let name: String?
    let url: String?
}
