//
//  PokemonListResponse.swift
//  Pokedex
//
//  Created by enzo corsiero on 16/01/21.
//

import Foundation

struct PokemonListResponse: Decodable {
    let next: String?
    let previus: String?
    let results: [PokemonItem]?
}

struct PokemonItem: Decodable {
    let name: String?
    let url: String?
}
