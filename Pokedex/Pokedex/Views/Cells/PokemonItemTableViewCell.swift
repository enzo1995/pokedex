//
//  PokemonItemTableViewCell.swift
//  Pokedex
//
//  Created by enzo corsiero on 16/01/21.
//

import Foundation
import UIKit

class PokemonItemTableViewCell: UITableViewCell {
    
    var pokemonImageView: UIImageView = {
        let imgView = UIImageView()
        imgView.contentMode = .scaleAspectFit
        imgView.clipsToBounds = true
        return imgView
    }()
    
    private var pokemonNameLabel : UILabel = {
        let label = UILabel()
        label.font = FontManager.sharedInstance.mediumFontWith(size: Constants.TextSize.Subtitle)
        label.textAlignment = .center
        label.numberOfLines = 2
        label.textColor = UIColor.textColor
        return label
    }()
    
    var onReuse: () -> Void = {}
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
     super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        self.addSubview(pokemonImageView)
        self.addSubview(pokemonNameLabel)
        
        pokemonImageView.anchor(top: self.topAnchor, left: self.leadingAnchor, bottom: self.bottomAnchor, paddingTop: 8, paddingLeft: 16, paddingBottom: 8, width: 50)
        pokemonNameLabel.anchor(left: self.pokemonImageView.trailingAnchor,right: self.trailingAnchor, paddingLeft: 8, paddingRight: 16, centerY: self.centerYAnchor)
        
     
     }
     
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func prepareForReuse() {
        self.pokemonImageView.image = nil
        self.pokemonImageView.cancelImageLoad()
    }
    
    func setData(pokemon: PokemonItem) {
        guard let name = pokemon.name else { return }
        self.pokemonNameLabel.text = name
        if let strings = pokemon.url?.components(separatedBy: "pokemon/"),
           strings.count == 2,
           let range: Range<String.Index> = strings[1].range(of: "/"),
           let url = URL(string: String(format: Constants.Urls.pokemonImage, String(strings[1].prefix(strings[1].distance(from: strings[1].startIndex, to: range.lowerBound))))){
            self.pokemonImageView.loadImage(at: url)
        }
    }
}
