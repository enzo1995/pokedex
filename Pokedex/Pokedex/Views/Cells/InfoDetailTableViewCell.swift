//
//  InfoDetailTableViewCell.swift
//  Pokedex
//
//  Created by enzo corsiero on 17/01/21.
//

import Foundation
import UIKit

class InfoDetailTableViewCell: UITableViewCell {
    
    private var titleLabel : UILabel = {
        let label = UILabel()
        label.font = FontManager.sharedInstance.regularFontWith(size: Constants.TextSize.Description)
        label.textAlignment = .left
        label.numberOfLines = 2
        label.textColor = UIColor.textColor
        return label
    }()
    
    private var detailLabel : UILabel = {
        let label = UILabel()
        label.font = FontManager.sharedInstance.mediumFontWith(size: Constants.TextSize.Description)
        label.textAlignment = .right
        label.numberOfLines = 2
        label.textColor = UIColor.textColor
        return label
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
     super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        self.addSubview(titleLabel)
        self.addSubview(detailLabel)
        
        titleLabel.anchor(left: self.leadingAnchor, paddingLeft: 16, centerY: self.centerYAnchor)
        detailLabel.anchor(left: self.titleLabel.trailingAnchor, right: self.trailingAnchor, paddingLeft: 16, paddingRight: 16, centerY: self.centerYAnchor)
        
        self.titleLabel.widthAnchor.constraint(equalTo: self.detailLabel.widthAnchor).isActive = true
             
     }
     
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    func setData(title: String, detail: String) {
        self.titleLabel.text = title
        self.detailLabel.text = detail
    }
}
