//
//  ImageTableViewCell.swift
//  Pokedex
//
//  Created by enzo corsiero on 17/01/21.
//

import Foundation
import UIKit

class ImageTableViewCell: UITableViewCell {
    
    var pokemonImageView: UIImageView = {
        let imgView = UIImageView()
        imgView.contentMode = .scaleAspectFit
        imgView.clipsToBounds = true
        return imgView
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
     super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        self.addSubview(pokemonImageView)
        
        pokemonImageView.anchor(top: self.topAnchor, left: self.leadingAnchor, bottom: self.bottomAnchor, right: self.trailingAnchor, paddingTop: 8, paddingLeft: 32, paddingBottom: 8, paddingRight: 32)
     
     }
     
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    func setData(image: UIImage) {
        self.pokemonImageView.image = image
    }
    
    func loadImage(from url: String) {
        if let url = URL(string: url) {
            self.pokemonImageView.loadImage(at: url)
        }
    }
}
