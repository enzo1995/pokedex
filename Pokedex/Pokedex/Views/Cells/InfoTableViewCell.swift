//
//  InfoTableViewCell.swift
//  Pokedex
//
//  Created by enzo corsiero on 17/01/21.
//

import Foundation
import UIKit

class InfoTableViewCell: UITableViewCell {
    
    private var detialLabel : UILabel = {
        let label = UILabel()
        label.font = FontManager.sharedInstance.regularFontWith(size: Constants.TextSize.Description)
        label.textAlignment = .center
        label.numberOfLines = 2
        label.textColor = UIColor.textColor
        return label
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
     super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        self.addSubview(detialLabel)
        
        detialLabel.anchor(left: self.leadingAnchor, right: self.trailingAnchor, paddingLeft: 16, paddingRight: 16, centerY: self.centerYAnchor)
             
     }
     
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    func setData(text: String) {
        self.detialLabel.text = text
    }
}
