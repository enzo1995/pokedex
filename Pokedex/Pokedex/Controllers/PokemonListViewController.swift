//
//  ViewController.swift
//  Pokedex
//
//  Created by enzo corsiero on 15/01/21.
//

import UIKit

class PokemonListViewController: UIViewController {
    
    private let pokemonCellId = "pokemonCellId"
    
    private var pokemonTableView: UITableView = {
        let tableView = UITableView()
        return tableView
    }()
    
    private var pokemonLoaded = [PokemonItem]()
    
    private var lastPokemonResponse: PokemonListResponse? = nil

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        self.view.backgroundColor = .white
        self.title = Localizer.Common.title.localized
        
        self.view.addSubview(self.pokemonTableView)
        self.pokemonTableView.anchor(top: self.view.topAnchor, left: self.view.leadingAnchor, bottom: self.view.bottomAnchor, right: self.view.trailingAnchor, paddingTop: 0, paddingLeft: 0, paddingBottom: 0, paddingRight: 0)
        pokemonTableView.delegate = self
        pokemonTableView.dataSource = self
        pokemonTableView.register(PokemonItemTableViewCell.self, forCellReuseIdentifier: pokemonCellId)
        
        loadPokemon()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.navigationBar.prefersLargeTitles = false
    }

    private func loadPokemon(url: String? = nil) {
        ApiManager.getPokemonList(url: url) { [weak self] response in
            guard let self = self else { return }
            self.lastPokemonResponse = response
            if let pokemonList = response?.results {
                pokemonList.forEach({self.pokemonLoaded.append($0)})
                self.pokemonTableView.reloadData()
            }
        }
    }
    
    private func loadOther() {
        if let nextUrl = lastPokemonResponse?.next {
            loadPokemon(url: nextUrl)
        }
    }

}

extension PokemonListViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        self.pokemonLoaded.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: pokemonCellId, for: indexPath) as? PokemonItemTableViewCell else { return UITableViewCell()}
        cell.setData(pokemon: self.pokemonLoaded[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.row == pokemonLoaded.count - 1 {
            self.loadOther()
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        70
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        guard indexPath.row <= self.pokemonLoaded.count - 1, let url = self.pokemonLoaded[indexPath.row].url else { return }
        ApiManager.getPokemonDetails(url: url) { [weak self] details in
            guard let self = self else { return }
            if let details = details {
                let pokemonDetailsVC = DetailsViewController(pokemonDetails: details)
                self.navigationController?.pushViewController(pokemonDetailsVC, animated: true)
            } else {
                SystemAlertMessage.standardAlert(title: Localizer.Alert.opsTitle.localized, message: Localizer.Alert.loadDetailError.localized, confirm: Localizer.Common.okay.localized)
            }
        }
        
        
    }
}
