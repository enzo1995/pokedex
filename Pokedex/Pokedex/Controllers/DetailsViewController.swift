//
//  DetailsViewController.swift
//  Pokedex
//
//  Created by enzo corsiero on 15/01/21.
//

import UIKit

class DetailsViewController: UIViewController {
    
    private let imageCellId = "imageCellId"
    private let detailCellId = "detailCellId"
    private let infoDetailCellId = "infoDetailCellId"
    
    private var dataTableView: UITableView = {
        let tableView = UITableView()
        tableView.allowsSelection = false
        return tableView
    }()
    
    private let pokemonDetails: PokemonDetailsResponse
    
    init(pokemonDetails: PokemonDetailsResponse) {
        self.pokemonDetails = pokemonDetails
        
        super.init(nibName: nil, bundle: nil)
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.backgroundColor = .white
        self.title = pokemonDetails.name

        self.view.addSubview(self.dataTableView)
        self.dataTableView.anchor(top: self.view.topAnchor, left: self.view.leadingAnchor, bottom: self.view.bottomAnchor, right: self.view.trailingAnchor, paddingTop: 0, paddingLeft: 0, paddingBottom: 0, paddingRight: 0)
        dataTableView.delegate = self
        dataTableView.dataSource = self
        
        dataTableView.register(ImageTableViewCell.self, forCellReuseIdentifier: imageCellId)
        dataTableView.register(InfoTableViewCell.self, forCellReuseIdentifier: detailCellId)
        dataTableView.register(InfoDetailTableViewCell.self, forCellReuseIdentifier: infoDetailCellId)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.navigationBar.prefersLargeTitles = true
    }

}

extension DetailsViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        4
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return 1
        case 1:
            return self.pokemonDetails.abilities?.count ?? 0
        case 2:
            return self.pokemonDetails.stats?.count ?? 0
        case 3:
            return self.pokemonDetails.moves?.count ?? 0
        default:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section {
        case 0:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: imageCellId, for: indexPath) as? ImageTableViewCell else { return UITableViewCell()}
            if let id = self.pokemonDetails.id {
                cell.loadImage(from: String(format: Constants.Urls.pokemonImage, String(id)))
            }
            return cell
        case 1:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: detailCellId, for: indexPath) as? InfoTableViewCell else { return UITableViewCell()}
            if let name = self.pokemonDetails.abilities?[indexPath.row].ability?.name {
                cell.setData(text: name)
            }
            return cell
        case 2:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: infoDetailCellId, for: indexPath) as? InfoDetailTableViewCell else { return UITableViewCell()}
            let item = self.pokemonDetails.stats?[indexPath.row]
            if let title = item?.stat?.name, let base = item?.base_stat, let effort = item?.effort {
                cell.setData(title: title, detail: "\(base) (+\(effort))")
            }
            return cell
        case 3:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: detailCellId, for: indexPath) as? InfoTableViewCell else { return UITableViewCell()}
            if let name = self.pokemonDetails.moves?[indexPath.row].move?.name {
                cell.setData(text: name)
            }
            return cell
        
        default:
            return UITableViewCell()
        }
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        switch section {
        case 0:
            return nil
        case 1:
            return Localizer.Details.abilities.localized
        case 2:
            return Localizer.Details.stats.localized
        case 3:
            return Localizer.Details.moves.localized
        default:
            return nil
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0 {
            return 250
        } else {
            return 50
        }
    }
    
    
}
